/*
 * This file contains methods for alle available commands.
 * Ever command accepts a message object, and a reference to the bot (Eris Client) instance.
 */

var StreamBuffers = require("stream-buffers");
var {createCanvas, loadImage} = require("canvas");
var Jimp = require("jimp");
var Request = require("request");

var config = require("./configLoader").load();

const apiUrl = "https://discordapp.com/api/v6/"; // Used for RESTful API requests

var commandHandler = {}




// Lists all available commands
commandHandler.help = function(bot, msg) {
	var toSend = "List of commands: ```" + config.prefix;
	toSend += Object.values(config.commands).join("\n" + config.prefix);
	toSend += "```These are not case-sensitive.";
	bot.createMessage(msg.channel.id, toSend);
}


// Checks if message contains a mention, and gets an avatar url from that user. 
// Otherwise checks if the message contains a valid user ID, and uses the RESTful API to get an avatar url. 
// Sends the generated url in the same channel as msg.
commandHandler.getAvatar = function(bot, msg) {
	var parts = msg.content.replace(/ +(?= )/g,'').split(" ");
	if (parts.length < 2) {
		bot.createMessage(msg.channel.id, "Gets the avatar from a user by mention or ID. \nSyntax: `" + config.prefix + config.commands.getAvatar + " (mention/user-ID)`");
		return;
	}
	
	// If getting avatar with a mention
	if (msg.mentions.length != 0) {
		// commandHandler.getAvatar(msg, msg.mentions[0].id); // TODO: Simplify, since avatar ID is already available
		bot.createMessage(msg.channel.id, avatarUrlFromHash(msg.mentions[0].id, msg.mentions[0].avatar));
		return;
	}
	
	// Else get by user ID
	// Checks inputs
	var userId = parts[1];
	// Checks if parameter is a valid id: 
	if (isNaN(parseInt(userId))) {
		bot.createMessage(msg.channel.id, "Invalid User ID");
		return;
	}
	avatarUrlFromUserId(userId).then(url => {
		bot.createMessage(msg.channel.id, url);
	}).catch(() => {
		bot.createMessage(msg.channel.id, "Unable to get avatar");
	});
}


// Retrieves the image url from an emoji (useful on mobile devices)
commandHandler.getEmojiUrl = function(bot, msg) {
	if (msg.content.trim().toLowerCase() === config.prefix + config.commands.getEmojiUrl.toLowerCase()) {
		bot.createMessage(msg.channel.id, "Gives you the URL to the image of a server emoji. \nSyntax: `" + 
			config.prefix + config.commands.getEmbedThumbnail + " (emoji)`\n"
		);
		return;
	}
	var index = msg.content.search(/\<\:[a-zA-Z0-9_-]+\:[0-9]+\>/);
	if (index == -1) {
		bot.createMessage(msg.channel.id, "No custom emoji found.");
		return;
	}
	var endIndex = msg.content.indexOf(">");
	var emoji = msg.content.substring(index, endIndex);
	var id = emoji.substring(emoji.lastIndexOf(":")+1);
	bot.createMessage(msg.channel.id, "https://cdn.discordapp.com/emojis/" + id + ".png?v=1");
}


// Retrieves Discord proxy url for image in an embed (e.g. Reddit previews).
commandHandler.getEmbedThumbnail = function(bot, msg) {
	if (msg.content.trim().toLowerCase() === config.prefix + config.commands.getEmbedThumbnail.toLowerCase()) {
		bot.createMessage(msg.channel.id, "Gets the proxy url of an embed, e.g. Reddit previews. \nSyntax: `" + 
			config.prefix + config.commands.getEmbedThumbnail + " (url)`\n"
			+ "If it doesn't work, try again, as Discord takes some time to generate an embed url."
		);
		return;
	}

	if (msg.embeds.length == 0) {
		bot.createMessage(msg.channel.id, "No embed found");
		return;
	}
	
	var url;
	try {
		url = msg.embeds[0].thumbnail.proxy_url;
	} 
	catch (e) {
		bot.createMessage(msg.channel.id, "Could not find image url.");
		return;
	}
	bot.createMessage(msg.channel.id, url);
}


// Creates a ring around an image, for use with avatars
commandHandler.makeRing = async function(bot, msg) {
	var url = "";
	// Split message for parameters etc
	var parts = msg.content.replace(/ +(?= )/g,'').split(" ");
	if (parts.length < 2) { // No parameters given, send syntax
		bot.createMessage(
			msg.channel.id, 
			"Syntax: \n`" + config.prefix + config.commands.makeRing 
			+ " (image) (colors) [thickness] [direction] [radius]`\n\n"
			+ "`(image)`: URL, mention, or user ID \n"
			+ "`(colors)`: Comma-seperated list of colors, or \"rainbow\" \n"
			+ "`[thickness]`: Optional: Line thickness, defaults to 16 \n"
			+ "`[direction]`: Comma-seperated list of four values between 0 and 1 to control gradient direction. Format: startX,startY,endX,endY. "
			+ "Defaults to Top-left to buttom-right (0,0,1,1). \n"
			+ "`[radius]`: Ring radius, defaults to half the image width."
		);
		return;
	}

	// If a user is mentioned, use their avatar
	if (msg.mentions.length != 0) {
		// commandHandler.getAvatar(msg, msg.mentions[0].id); // TODO: Simplify, since avatar ID is already available
		url = avatarUrlFromHash(msg.mentions[0].id, msg.mentions[0].avatar);
	}
	else if (!isNaN(parseInt(parts[1]))) { // User provided a user ID
		url = await avatarUrlFromUserId(parts[1].trim());
	}
	else {
		url = parts[1].trim();
	}

	var colors = [];
	if (parts.length < 2) {
		bot.createMessage(msg.channel.id, "Not enough arguments. ");
	}
	else {
		var temp = parts[2].split(",");
		if (temp.length == 1 && temp[0] == "rainbow") { // User requested rainbow
			colors = ["#FF0000", "#FF8800", "#FFFF00", "#00FF00", "#0044FF", "#8800FF", "#FF00FF"];
		}
		else {
			// Validate colors
			for (var i = 0; i < temp.length; i++) {
				if (!/#?[0-9A-Fa-f]{6}/.test(temp[i])) {
					bot.createMessage(msg.channel.id, "Invalid color/gradient");
					return;
				}
				if (temp[i].startsWith("#")) {
					colors.push(temp[i].toUpperCase());
				}
				else {
					colors.push("#" + temp[i].toUpperCase());
				}
			}
		}
	}
	var lineWidth = 16;
	if (parts.length >= 4) {
		var newLineWidth = parseInt(parts[3].trim());
		if (!isNaN(newLineWidth) && newLineWidth >= 0) {
			lineWidth = newLineWidth;
		}
	}
	var direction = [0, 0, 1, 1];
	if (parts.length >= 5) {
		var newDirections = parts[4].split(",");
		if (newDirections.length != 4) {
			bot.createMessage(msg.channel.id, "You must provide exactly 4 direction values (or none)");
			return;
		}
		direction = newDirections.map(v => parseFloat(v));
		for (var i = 0; i < direction.length; i++) {
			if (isNaN(direction[i]) || direction[i] < 0 || direction[i] > 1) {
				bot.createMessage(msg.channel.id, "Invalid direction provided: " + newDirections[i]);
				return;
			}
		}
	}
	var radius = -1; 
	if (parts.length >= 6) {
		var temp = parseFloat(parts[5].trim());
		if (!isNaN(temp) && temp >= 0) {
			radius = temp;
		} 
	}

	loadImage(url).then((png) => {
		var canvas = createCanvas(png.width, png.height);
		var ctx = canvas.getContext("2d");
		ctx.drawImage(png, 0, 0);
		if (colors.length > 1) {
			var gradient = ctx.createLinearGradient(png.width*direction[0], png.height*direction[1], png.width*direction[2], png.height*direction[3]);
			for (var i = 0; i < colors.length; i++) {
				gradient.addColorStop(i/(colors.length-1), colors[i]);
			}
			ctx.strokeStyle = gradient;
		} 
		else {
			ctx.strokeStyle = colors[0];
		}
		ctx.lineWidth = lineWidth;
		ctx.beginPath();
		radius = (radius == -1) ? png.width/2 : radius;
		ctx.arc(png.width/2, png.height/2, radius, 0, 2*Math.PI);
		ctx.closePath();
		ctx.stroke();
		var bufferToSend = new StreamBuffers.WritableStreamBuffer();
		canvas.createPNGStream().pipe(bufferToSend);
		bufferToSend.on("finish", () => {
			bot.createMessage(msg.channel.id, "I tried my best, please don't hurt me", {
				name: "ringed.png",
				file: bufferToSend.getContents()
			});
		});
	}).catch(() => {
		bot.createMessage(msg.channel.id, "Invalid url");
	});
}


// Scales an image
commandHandler.resizeImage = function(bot, msg) {
	var parts = msg.content.replace(/ +(?= )/g,'').split(" ");

	// Prints help
	if (parts.length < 2) {
		bot.createMessage(
			msg.channel.id,
			"Syntax: \n`" + config.prefix + config.commands.resizeImage + " [image url] (format/resolution)`\n\n" 
			+ "`[image url]`: URL to the image you want to resize. Can be omitted if you attach an image.\n" 
			+ "`(format/resolution)`: How you want the image to be resized. E.g. 16:9, 300x200. "
		);
		return;
	}

	else if (!(parts.length == 3 || (parts.length == 2 && msg.attachments.length == 1))) {
		bot.createMessage(msg.channel.id, "Invalid command usage.");
		return;
	}

	var newRes;
	var resOrRat = parts.pop();
	// Checks if user specified a resolution (two positive integers seperated by x)
	if (/[1-9][0-9]*x[1-9][0-9]*/.test(resOrRat)) {
		newRes = {};
		resOrRat = resOrRat.split("x");
		newRes.w = parseInt(resOrRat[0]);
		newRes.h = parseInt(resOrRat[1]);
		if (newRes.w * newRes.h > 1000000) {
			bot.createMessage(msg.channel.id, "Resolution too high, please don't kill my server.");
			return;
		}
	}
	// If they didn't specify a resolution, they HAVE to specify an aspect ratio (two positive integers seperated by :)
	else if (!/[1-9][0-9]*:[1-9][0-9]*/.test(resOrRat)) {
		bot.createMessage(msg.channel.id, "Invalid resolution/ratio");
		return;
	}

	var imageUrl = "";
	if (msg.attachments.length != 0) imageUrl = msg.attachments[0].url;
	else imageUrl = parts[1];

	Jimp.read(imageUrl)
		.then(img => {
			// If newRes hasn't been defined, the user specified an aspect ratio. Calculate new x and y
			if (newRes == undefined) {
				newRes = {};
				resOrRat = resOrRat.split(":");
				var tempW = parseInt(resOrRat[0]);
				var tempH = parseInt(resOrRat[1]);
				// Try to only resize one side
				if (img.getWidth()/tempW < img.getHeight()/tempH) {
					newRes.w = img.getHeight() * tempW/tempH;
					newRes.h = img.getWidth();
				}
				else {
					newRes.w = img.getHeight();
					newRes.h = img.getWidth() * tempH/tempW;
				}
				
				if (newRes.w * newRes.h > 1000000) {
					bot.createMessage(msg.channel.id, "Resolution too high, please don't kill my server.");
					return;
				}
			}
			img.resize(newRes.w, newRes.h)
			.getBufferAsync(img.getMIME())
				.then(buffer => {
					bot.createMessage(msg.channel.id, "Here's your image, hope you like it.", {
						name: "resized." + img.getMIME().split("/").pop(), // Appends the right extension
						file: buffer
					});
				})
				.catch(err => {
					bot.createMessage(msg.channel.id, "Error resizing image");
				})
		})
		.catch(err => {
			bot.createMessage(msg.channel.id, "Error getting image");
		});
}


// Generate an image filled with a single color
commandHandler.viewColor = function(bot, msg) {
	var parts = msg.content.replace(/ +(?= )/g,'').split(" ");
	if (parts.length < 2) {
		bot.createMessage(msg.channel.id, "Generates a 200x200 image filled with a solid color. \nSyntax: `" + config.prefix + config.commands.viewColor + " (hex color)`");
		return;
	}
	var color = parts[1];
	if (!/#?[0-9A-Fa-f]{6}/.test(color)) {
		bot.createMessage(msg.channel.id, "Invalid color");
		return;
	}
	if (!color.startsWith("#")) {
		color = "#" + color;
	}
	color = color.toUpperCase();
	var canvas = createCanvas(200, 200);
	var ctx = canvas.getContext("2d");
	ctx.fillStyle = color;
	ctx.fillRect(0, 0, 200, 200);
	var bufferToSend = new StreamBuffers.WritableStreamBuffer();
	canvas.createPNGStream().pipe(bufferToSend);
	bufferToSend.on("finish", () => {
		bot.createMessage(msg.channel.id, "Here's your color:", {
			name: "color.png",
			file: bufferToSend.getContents()
		});
	});
}


module.exports = commandHandler;




// Various helper functions for commands. Might move to a different file later
function avatarUrlFromHash(userId, hash) {
	return "https://cdn.discordapp.com/avatars/" + userId + "/" + hash + ".png"
}



function avatarUrlFromUserId(id) {
	return new Promise((resolve, reject) => {
		const options = {
			url: apiUrl + "users/" + id,
			headers: {
				"Authorization": "Bot " + config.token
			}
		}
		Request.get(options, (err, res, body) => {
			if (err || res.statusCode != 200) {
				reject("Unable to get avatar");
			}
			body = JSON.parse(body);
			resolve(avatarUrlFromHash(id, body.avatar));
		});
	})
}





