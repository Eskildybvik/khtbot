var fs = require("fs");

loader = {};
loader.load = function() {
	var defaultConfig = JSON.parse(fs.readFileSync("./config-default.json"));
	var userConfig = JSON.parse(fs.readFileSync("./config.json"));

	var configObject = {};
	configObject.token = userConfig.token;
	configObject.prefix = userConfig.prefix != undefined ? userConfig.prefix : defaultConfig.prefix;

	configObject.commands = {};
	if (userConfig.commands != undefined) {
		configObject.commands.isUp = userConfig.commands.isUp != undefined ? userConfig.commands.isUp : defaultConfig.commands.isUp;
		configObject.commands.help = userConfig.commands.help != undefined ? userConfig.commands.help : defaultConfig.commands.help;
		configObject.commands.getAvatar = userConfig.commands.getAvatar != undefined ? userConfig.commands.getAvatar : defaultConfig.commands.getAvatar;
		configObject.commands.getEmbedThumbnail = userConfig.commands.getEmbedThumbnail != undefined ? userConfig.commands.getEmbedThumbnail : defaultConfig.commands.getEmbedThumbnail;
		configObject.commands.makeRing = userConfig.commands.makeRing != undefined ? userConfig.commands.makeRing : defaultConfig.commands.makeRing;
		configObject.commands.resizeImage = userConfig.commands.resizeImage != undefined ? userConfig.commands.resizeImage : defaultConfig.commands.resizeImage;
		configObject.commands.viewColor = userConfig.commands.viewColor != undefined ? userConfig.commands.viewColor : defaultConfig.commands.viewColor;
		configObject.commands.getEmojiUrl = userConfig.commands.getEmojiUrl != undefined ? userConfig.commands.getEmojiUrl : defaultConfig.commands.getEmojiUrl;
	}
	else {
		configObject.commands = defaultConfig.commands;
	}

	return configObject;
}

module.exports = loader;