var Eris = require("eris");


var config = require("./configLoader").load();
var commandHandler = require("./commandHandler");

var bot = new Eris(config.token);

bot.on("ready", () => {
	console.log("Connected!");
});

bot.on("messageCreate", (msg) => {
	if (!msg.content.startsWith(config.prefix)) return;
	var text = msg.content.substring(config.prefix.length); // Cuts off the prefix

	// Register every command
	// Simple ping command
	if (text.toLowerCase().startsWith(config.commands.isUp.toLowerCase())) {
		// No need to register in commandHandler.
		bot.createMessage(msg.channel.id, "I am up.");
	}

	else if (text.toLowerCase().startsWith(config.commands.help.toLowerCase())) {
		commandHandler.help(bot, msg);
	}

	else if (text.toLowerCase().startsWith(config.commands.getAvatar.toLowerCase())) {
		commandHandler.getAvatar(bot, msg);
	}

	else if (text.toLowerCase().startsWith(config.commands.getEmbedThumbnail.toLowerCase())) {
		commandHandler.getEmbedThumbnail(bot, msg);
	}

	else if (text.toLowerCase().startsWith(config.commands.makeRing.toLowerCase())) {
		commandHandler.makeRing(bot, msg);
	}

	else if (text.toLowerCase().startsWith(config.commands.resizeImage.toLowerCase())) {
		commandHandler.resizeImage(bot, msg);
	}

	else if (text.toLowerCase().startsWith(config.commands.viewColor.toLowerCase())) {
		commandHandler.viewColor(bot, msg);
	}

	else if (text.toLowerCase().startsWith(config.commands.getEmojiUrl.toLowerCase())) {
		commandHandler.getEmojiUrl(bot, msg);
	}
});


bot.connect();